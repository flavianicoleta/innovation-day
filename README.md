# README #

### What is this repository for? ###

* A proof of concept to showcase some of web sockets capabilities. It uses the Spring support for web sockets together with SockJS and STOMP protocol


### How do I get set up? ###

* Clone the project
* Open a terminal and go to folder location
* mvn tomcat7:run
* Go to: http://localhost:9200/movie-center/login.html
* Users: 
   * * User: "admin", Password: "admin"
   * * User: "flavia", Password: "flavia"
   * * User: "john", Password: "doe"